Rails.application.routes.draw do
  resources :evaluatees
  namespace :admin do
    DashboardManifest::DASHBOARDS.each do |dashboard_resource|
      resources dashboard_resource
    end

    root controller: DashboardManifest::ROOT_DASHBOARD, action: :index
  end

  root to: 'visitors#index'
  devise_for :users  
  resources :users

  resources :evaluations

  resources :evaluators

  resources :subjects

  resources :evaluation_templates

  resources :ratings

  resources :comments

  resources :evaluation_templates_topics

  resources :rating_templates

  resources :topics

  post 'evaluators/select' => "evaluators#select"

  get 'send_email' => "evaluators#send_mail", as: 'send_mail'
  get "evaluations/:evaluation_id/preview" => "evaluations#preview", as: 'evaluation_preview'
end
