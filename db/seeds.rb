# Comment.create!([
#   {type: "EvaluationComment", topic_id: 5, text: "Weak left foot", evaluation_id: 1, evaluation_template_id: nil},
#   {type: "EvaluationComment", topic_id: 6, text: "Improve on receiving balls in the air", evaluation_id: 1, evaluation_template_id: nil},
#   {type: "TemplateComment", topic_id: 5, text: "Inside foot passing", evaluation_id: nil, evaluation_template_id: 1},
#   {type: "TemplateComment", topic_id: 5, text: "Outside foot passing", evaluation_id: nil, evaluation_template_id: 1},
#   {type: "TemplateComment", topic_id: 5, text: "Driven passes", evaluation_id: nil, evaluation_template_id: 1},
#   {type: "TemplateComment", topic_id: 6, text: "Balls on the ground", evaluation_id: nil, evaluation_template_id: 1},
#   {type: "TemplateComment", topic_id: 6, text: "Balls out of the air", evaluation_id: nil, evaluation_template_id: 1}
# ])
# Evaluation.create!([
#   {evaluation_template_id: 1, evaluator_id: 1, subject_id: 1, evaluation_date: "2011-05-24"},
#   {evaluation_template_id: 1, evaluator_id: 1, subject_id: 1, evaluation_date: "2011-03-12"},
#   {evaluation_template_id: 1, evaluator_id: 1, subject_id: 1, evaluation_date: "2015-12-07"}
# ])
# EvaluationTemplate.create!([
#   {name: "Player Evaluation", logo: nil},
#   {name: "Team Evaluation", logo: nil}
# ])
# EvaluationTemplate::HABTM_Topics.create!([
#   {evaluation_template_id: 1, topic_id: 1},
#   {evaluation_template_id: 1, topic_id: 2},
#   {evaluation_template_id: 1, topic_id: 3},
#   {evaluation_template_id: 1, topic_id: 4}
# ])
# EvaluationTemplatesTopic.create!([
#   {evaluation_template_id: 1, topic_id: 1},
#   {evaluation_template_id: 1, topic_id: 2},
#   {evaluation_template_id: 1, topic_id: 3},
#   {evaluation_template_id: 1, topic_id: 4}
# ])
# Evaluator.create!([
#   {name: "Coach Means"},
#   {name: "Billy Shocker"}
# ])
# Rating.create!([
#   {evaluation_id: 1, topic_id: 16, value: "2.9"},
#   {evaluation_id: 2, topic_id: 9, value: "4.3"},
#   {evaluation_id: 2, topic_id: 16, value: "3.6"},
#   {evaluation_id: 2, topic_id: 12, value: "3.1"},
#   {evaluation_id: 1, topic_id: 12, value: "3.2"},
#   {evaluation_id: 2, topic_id: 5, value: "3.4"},
#   {evaluation_id: 1, topic_id: 7, value: "3.1"},
#   {evaluation_id: 1, topic_id: 19, value: "4.5"},
#   {evaluation_id: 2, topic_id: 19, value: "4.8"},
#   {evaluation_id: 2, topic_id: 6, value: "3.8"},
#   {evaluation_id: 1, topic_id: 15, value: "4.4"},
#   {evaluation_id: 2, topic_id: 15, value: "4.4"},
#   {evaluation_id: 2, topic_id: 11, value: "3.0"},
#   {evaluation_id: 1, topic_id: 8, value: "4.2"},
#   {evaluation_id: 1, topic_id: 11, value: "3.0"},
#   {evaluation_id: 1, topic_id: 5, value: "3.2"},
#   {evaluation_id: 2, topic_id: 18, value: "3.4"},
#   {evaluation_id: 2, topic_id: 7, value: "3.0"},
#   {evaluation_id: 1, topic_id: 18, value: "3.5"},
#   {evaluation_id: 1, topic_id: 9, value: "4.1"},
#   {evaluation_id: 1, topic_id: 10, value: "2.9"},
#   {evaluation_id: 2, topic_id: 10, value: "3.2"},
#   {evaluation_id: 2, topic_id: 14, value: "4.5"},
#   {evaluation_id: 1, topic_id: 14, value: "4.1"},
#   {evaluation_id: 1, topic_id: 13, value: "3.7"},
#   {evaluation_id: 2, topic_id: 13, value: "3.5"},
#   {evaluation_id: 2, topic_id: 8, value: "4.1"},
#   {evaluation_id: 2, topic_id: 17, value: "3.5"},
#   {evaluation_id: 1, topic_id: 17, value: "3.3"},
#   {evaluation_id: 1, topic_id: 6, value: "3.6"}
# ])
# RatingTemplate.create!([
#   {low: 0, high: 5, low_description: "poor", high_description: "excellent"},
#   {low: 1, high: 10, low_description: "worst", high_description: "best"}
# ])
# Subject.create!([
#   {name: "Billy Balls"},
#   {name: "Lionel Messi"}
# ])
# Topic.create!([
#   {rating_template_id: 1, title: "Technical", description: "Technique", parent_id: 0, lft: 5, rgt: 8},
#   {rating_template_id: 1, title: "Tactical", description: "Tactics", parent_id: 0, lft: 17, rgt: 19},
#   {rating_template_id: 1, title: "Physical", description: "Physio", parent_id: 0, lft: 9, rgt: 13},
#   {rating_template_id: 1, title: "Psychological", description: "Physcho", parent_id: 0, lft: 14, rgt: 16},
#   {rating_template_id: 1, title: "Passing", description: "MyText", parent_id: 1, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Receiving", description: "MyText", parent_id: 1, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Dribbling", description: "MyText", parent_id: 1, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Shooting", description: "MyText", parent_id: 1, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Strength and Power", description: "MyText", parent_id: 3, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Speed", description: "MyText", parent_id: 3, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Agility", description: "MyText", parent_id: 3, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Quickness", description: "MyText", parent_id: 3, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Endurance", description: "Cardiovascular endurance/aerobic fitness", parent_id: 3, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Mental Toughness", description: "MyText", parent_id: 4, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Leadership", description: "MyText", parent_id: 4, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Attitude", description: "Attitude towards self, teammates and coaches", parent_id: 4, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Decision Making", description: "MyText", parent_id: 2, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Vision", description: "MyText", parent_id: 2, lft: 0, rgt: 0},
#   {rating_template_id: 1, title: "Role", description: "Understands role and function in various situations and locations on the field", parent_id: 2, lft: 0, rgt: 0}
# ])
# Topic::HABTM_EvaluationTemplates.create!([
#   {evaluation_template_id: 1, topic_id: 1},
#   {evaluation_template_id: 1, topic_id: 2},
#   {evaluation_template_id: 1, topic_id: 3},
#   {evaluation_template_id: 1, topic_id: 4}
# ])
# EvaluationComment.create!([
#   {type: "EvaluationComment", topic_id: 5, text: "Weak left foot", evaluation_id: 1, evaluation_template_id: nil},
#   {type: "EvaluationComment", topic_id: 6, text: "Improve on receiving balls in the air", evaluation_id: 1, evaluation_template_id: nil}
# ])
# TemplateComment.create!([
#   {type: "TemplateComment", topic_id: 5, text: "Inside foot passing", evaluation_id: nil, evaluation_template_id: 1},
#   {type: "TemplateComment", topic_id: 5, text: "Outside foot passing", evaluation_id: nil, evaluation_template_id: 1},
#   {type: "TemplateComment", topic_id: 5, text: "Driven passes", evaluation_id: nil, evaluation_template_id: 1},
#   {type: "TemplateComment", topic_id: 6, text: "Balls on the ground", evaluation_id: nil, evaluation_template_id: 1},
#   {type: "TemplateComment", topic_id: 6, text: "Balls out of the air", evaluation_id: nil, evaluation_template_id: 1}
# ])
