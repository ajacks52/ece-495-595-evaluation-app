# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151208033651) do

  create_table "comments", force: :cascade do |t|
    t.string   "type"
    t.integer  "topic_id"
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "evaluation_id"
    t.integer  "evaluation_template_id"
    t.integer  "evaluatee_id"
    t.integer  "evaluator_id"
  end

  add_index "comments", ["evaluatee_id"], name: "index_comments_on_evaluatee_id"
  add_index "comments", ["evaluator_id"], name: "index_comments_on_evaluator_id"

  create_table "evaluatees", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.string   "description"
    t.integer  "evaluator_id"
  end

  add_index "evaluatees", ["evaluator_id"], name: "index_evaluatees_on_evaluator_id"

  create_table "evaluation_templates", force: :cascade do |t|
    t.string   "name"
    t.binary   "logo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "evaluation_templates_topics", id: false, force: :cascade do |t|
    t.integer  "evaluation_template_id", null: false
    t.integer  "topic_id",               null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "evaluations", force: :cascade do |t|
    t.integer  "evaluation_template_id"
    t.integer  "evaluator_id"
    t.integer  "subject_id"
    t.date     "evaluation_date"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "evaluatee_id"
  end

  add_index "evaluations", ["evaluatee_id"], name: "index_evaluations_on_evaluatee_id"

  create_table "evaluators", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rating_templates", force: :cascade do |t|
    t.integer  "low"
    t.integer  "high"
    t.text     "low_description"
    t.text     "high_description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "ratings", force: :cascade do |t|
    t.integer  "evaluation_id"
    t.integer  "topic_id"
    t.decimal  "value"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "evaluatee_id"
    t.integer  "evaluator_id"
  end

  add_index "ratings", ["evaluatee_id"], name: "index_ratings_on_evaluatee_id"
  add_index "ratings", ["evaluator_id"], name: "index_ratings_on_evaluator_id"

  create_table "subjects", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "topics", force: :cascade do |t|
    t.integer  "rating_template_id"
    t.string   "title"
    t.text     "description"
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.integer  "role"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true

end
