class AddEvaluateeAndEvaluatorToComments < ActiveRecord::Migration
  def change
    add_reference :comments, :evaluatee, index: true, foreign_key: true
    add_reference :comments, :evaluator, index: true, foreign_key: true
  end
end
