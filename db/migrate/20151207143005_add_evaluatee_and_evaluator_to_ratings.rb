class AddEvaluateeAndEvaluatorToRatings < ActiveRecord::Migration
  def change
    add_reference :ratings, :evaluatee, index: true, foreign_key: true
    add_reference :ratings, :evaluator, index: true, foreign_key: true
  end
end
