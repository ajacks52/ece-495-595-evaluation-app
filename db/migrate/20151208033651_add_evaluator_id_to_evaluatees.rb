class AddEvaluatorIdToEvaluatees < ActiveRecord::Migration
  def change
    add_reference :evaluatees, :evaluator, index: true #, foreign_key: true
  end
end
