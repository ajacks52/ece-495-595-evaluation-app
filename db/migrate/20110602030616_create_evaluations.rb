class CreateEvaluations < ActiveRecord::Migration
  def self.up
    create_table :evaluations do |t|
      t.integer :evaluation_template_id
      t.integer :evaluator_id
      t.integer :subject_id
      t.date :evaluation_date

      t.timestamps
    end
  end

  def self.down
    drop_table :evaluations
  end
end
