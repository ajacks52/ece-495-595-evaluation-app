class CreateEvaluationTemplatesTopics < ActiveRecord::Migration
  def self.up
    create_table :evaluation_templates_topics, :id => false do |t| # join table, so no primary key
      t.integer :evaluation_template_id, :null=>false  # foreign key
      t.integer :topic_id, :null=>false  # foreign key

      t.timestamps
    end
  end

  def self.down
    drop_table :evaluation_templates_topics
  end
end
