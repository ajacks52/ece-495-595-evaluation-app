class AddDescriptionToEvaluatees < ActiveRecord::Migration
  def change
    add_column :evaluatees, :description, :string
  end
end
