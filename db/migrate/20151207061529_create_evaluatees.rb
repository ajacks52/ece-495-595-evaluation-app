class CreateEvaluatees < ActiveRecord::Migration
  def change
    create_table :evaluatees do |t|
      t.string :name
      t.string :email

      t.timestamps null: false
    end
  end
end
