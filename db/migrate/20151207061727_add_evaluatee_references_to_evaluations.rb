class AddEvaluateeReferencesToEvaluations < ActiveRecord::Migration
  def change
    add_reference :evaluations, :evaluatee, index: true#, foreign_key: true
  end
end
