class CreateEvaluationTemplates < ActiveRecord::Migration
  def self.up
    create_table :evaluation_templates do |t|
      t.string :name
      t.binary :logo

      t.timestamps
    end
  end

  def self.down
    drop_table :evaluation_templates
  end
end
