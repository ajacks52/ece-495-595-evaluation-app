class CreateComments < ActiveRecord::Migration
  def self.up
    create_table :comments do |t|
      
      # common attributes
      t.string :type
      t.integer :topic_id
      t.text :text
      t.timestamps
      
      # attributes for type=EvaluationComment      
      t.integer :evaluation_id
      
      # attributes for type=TemplateComment
      t.integer :evaluation_template_id
      
    end
  end

  def self.down
    drop_table :comments
  end
end
