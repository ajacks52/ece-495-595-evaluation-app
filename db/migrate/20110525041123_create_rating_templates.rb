class CreateRatingTemplates < ActiveRecord::Migration
  def self.up
    create_table :rating_templates do |t|
      t.integer :low
      t.integer :high
      t.text :low_description
      t.text :high_description

      t.timestamps
    end
  end

  def self.down
    drop_table :rating_templates
  end
end
