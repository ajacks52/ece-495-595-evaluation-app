# == Schema Information
#
# Table name: topics
#
#  id                 :integer          not null, primary key
#  rating_template_id :integer
#  title              :string
#  description        :text
#  parent_id          :integer
#  lft                :integer
#  rgt                :integer
#  created_at         :datetime
#  updated_at         :datetime
#

class Topic < ActiveRecord::Base
  acts_as_nested_set
  
  has_and_belongs_to_many :evaluation_templates
  has_many :ratings
  has_many :comments
  belongs_to :rating_template
  
  def previous_ratings(evaluation_template_id, subject_id)
    prev_rating = []
    Evaluation.where(:evaluation_template_id => evaluation_template_id, :subject_id => subject_id).each do |eval|
      rating = Rating.where(:evaluation_id => eval.id, :topic_id => self.id).first 
      prev_rating << [rating.value, eval.evaluation_date] unless rating == nil
    end  
    prev_rating 
  end
  
end
