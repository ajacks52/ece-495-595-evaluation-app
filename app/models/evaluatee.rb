# == Schema Information
#
# Table name: evaluatees
#
#  id           :integer          not null, primary key
#  name         :string
#  email        :string
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  description  :string
#  evaluator_id :integer
#

class Evaluatee < ActiveRecord::Base
  has_many :evaluations
  belongs_to :evaluator, :class_name => 'User', :foreign_key => 'evaluator_id'
end
