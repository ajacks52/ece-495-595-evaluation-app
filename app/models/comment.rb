# == Schema Information
#
# Table name: comments
#
#  id                     :integer          not null, primary key
#  type                   :string
#  topic_id               :integer
#  text                   :text
#  created_at             :datetime
#  updated_at             :datetime
#  evaluation_id          :integer
#  evaluation_template_id :integer
#  evaluatee_id           :integer
#  evaluator_id           :integer
#

class Comment < ActiveRecord::Base
  belongs_to :topic
end
