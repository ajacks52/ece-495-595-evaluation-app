# == Schema Information
#
# Table name: evaluators
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

class Evaluator < ActiveRecord::Base
  has_many :evaluations
  has_many :evaluatees
end
