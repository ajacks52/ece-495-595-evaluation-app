# == Schema Information
#
# Table name: evaluation_templates
#
#  id         :integer          not null, primary key
#  name       :string
#  logo       :binary
#  created_at :datetime
#  updated_at :datetime
#

class EvaluationTemplate < ActiveRecord::Base
  has_and_belongs_to_many :topics

  validates :name, presence: true
end
