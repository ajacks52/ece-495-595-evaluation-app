# == Schema Information
#
# Table name: ratings
#
#  id            :integer          not null, primary key
#  evaluation_id :integer
#  topic_id      :integer
#  value         :decimal(, )
#  created_at    :datetime
#  updated_at    :datetime
#  evaluatee_id  :integer
#  evaluator_id  :integer
#

class Rating < ActiveRecord::Base
  belongs_to :evaluation
  belongs_to :topic
  belongs_to :evaluator
  belongs_to :evaluatee
end
