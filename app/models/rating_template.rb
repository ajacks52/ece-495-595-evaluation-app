# == Schema Information
#
# Table name: rating_templates
#
#  id               :integer          not null, primary key
#  low              :integer
#  high             :integer
#  low_description  :text
#  high_description :text
#  created_at       :datetime
#  updated_at       :datetime
#

class RatingTemplate < ActiveRecord::Base
  has_many :topics
end
