# == Schema Information
#
# Table name: evaluations
#
#  id                     :integer          not null, primary key
#  evaluation_template_id :integer
#  evaluator_id           :integer
#  subject_id             :integer
#  evaluation_date        :date
#  created_at             :datetime
#  updated_at             :datetime
#  evaluatee_id           :integer
#

class Evaluation < ActiveRecord::Base
  belongs_to :evaluation_template
  belongs_to :evaluator, :class_name => 'User', :foreign_key => 'evaluator_id'
  belongs_to :subjects
  belongs_to :evaluatee

  has_many :ratings
  has_many :comments
end
