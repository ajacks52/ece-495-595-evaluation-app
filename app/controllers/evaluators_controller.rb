class EvaluatorsController < ApplicationController
  before_filter :authenticate_evaluator
  before_filter :set_evaluator, only: [:show, :edit, :update, :destroy]

  # GET /evaluators
  # GET /evaluators.xml
  def index
    @evaluators = Evaluator.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @evaluators }
    end
  end

  # GET /evaluators/1
  # GET /evaluators/1.xml
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @evaluator }
    end
  end

  # GET /evaluators/new
  # GET /evaluators/new.xml
  def new
    @evaluator = Evaluator.new()

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @evaluator }
    end
  end

  # GET /evaluators/1/edit
  def edit
  end

  # POST /evaluators
  # POST /evaluators.xml
  def create
    @evaluator = Evaluator.new(evaluator_params)

    respond_to do |format|
      if @evaluator.save
        format.html { redirect_to(@evaluator, :notice => 'Evaluator was successfully created.') }
        format.xml  { render :xml => @evaluator, :status => :created, :location => @evaluator }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @evaluator.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /evaluators/1
  # PUT /evaluators/1.xml
  def update
    respond_to do |format|
      if @evaluator.update_attributes(evaluator_params)
        format.html { redirect_to(@evaluator, :notice => 'Evaluator was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @evaluator.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /evaluators/1
  # DELETE /evaluators/1.xml
  def destroy
    @evaluator.destroy

    respond_to do |format|
      format.html { redirect_to(evaluators_url) }
      format.xml  { head :ok }
    end
  end


  # post evaluators/select
  def select
     query_params = {
         :evaluator_id => current_user.id,
         :evaluation_template_id => params[:evaluation_template],
         :evaluatee_id => params[:evaluatee]
     }

     evaluation = Evaluation.where(query_params).first

     evaluation = Evaluation.create(query_params) unless evaluation

    redirect_to evaluation_path(evaluation, evaluatee_id: params[:evaluatee], evaluation_template_id: params[:evaluation_template])
  end


  def send_mail
    Pony.options = {
        :via => :smtp,
        :via_options => {
            :address => 'smtp.sendgrid.net',
            :port => '587',
            :domain => 'heroku.com',
            # :domain => 'localhost:3000',
            :user_name => 'app44753449@heroku.com',
            :password => 'qizymckv3375',
            :authentication => :plain,
            :enable_starttls_auto => true
        }
    }

    Pony.mail(
        :to => params[:evaluatee_email],
        :html_body => "<h2>Welcome to online evaluation</h2><br>You can go to below url to view your evaluation: https://online-evaluation.herokuapp.com/evaluations/#{params[:evaluation_id]}/preview",
        :subject => 'Your evaluation page'
    )
  end

  private
  def set_evaluator
    @evaluator = Evaluator.find(params[:id])
  end

  def evaluator_params
    params.require(:evaluator).permit(:name)
  end
end
