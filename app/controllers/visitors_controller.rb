class VisitorsController < ApplicationController
  def index
    unless current_user
      redirect_to new_user_session_path
    end

    if current_user.try(:admin?)
      redirect_to evaluation_templates_path
    end

  end
end
