class EvaluationTemplatesController < ApplicationController
  before_filter :authenticate_admin
  before_filter :set_evaluation_template, only: [:show, :edit, :update, :destroy]

  # GET /evaluation_templates
  # GET /evaluation_templates.xml
  def index
    @evaluation_templates = EvaluationTemplate.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @evaluation_templates }
    end
  end

  # GET /evaluation_templates/1
  # GET /evaluation_templates/1.xml
  def show
    @subject = Subject.new(:name => "<Name>")
    @evaluator = Evaluator.new(:name => "<Evaluator>")
    @evaluation = Evaluation.new(:evaluation_date => Date.today)
    @topics = @evaluation_template.topics.where(parent_id: nil)

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @evaluation_template }
    end
  end

  # GET /evaluation_templates/new
  # GET /evaluation_templates/new.xml
  def new
    @evaluation_template = EvaluationTemplate.new
    @topic = Topic.new
    @topics = Topic.all


    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @evaluation_template }
    end
  end

  # GET /evaluation_templates/1/edit
  def edit
    @topics = @evaluation_template.topics.where(parent_id: nil)

    @topic = Topic.new

  end

  # POST /evaluation_templates
  # POST /evaluation_templates.xml
  def create
    @evaluation_template = EvaluationTemplate.new(evaluation_template_params)

    respond_to do |format|
      if @evaluation_template.save
        if params[:topic_ids]
          @evaluation_template.topics = Topic.where(id: params[:topic_ids])
          @evaluation_template.save
        end

        format.html { redirect_to edit_evaluation_template_path(@evaluation_template), :notice => 'Evaluation template was successfully created.' }
        format.xml  { render :xml => @evaluation_template, :status => :created, :location => @evaluation_template }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @evaluation_template.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /evaluation_templates/1
  # PUT /evaluation_templates/1.xml
  def update
    respond_to do |format|
      if @evaluation_template.update_attributes(evaluation_template_params)
        format.html { redirect_to(@evaluation_template, :notice => 'Evaluation template was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @evaluation_template.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /evaluation_templates/1
  # DELETE /evaluation_templates/1.xml
  def destroy
    @evaluation_template.destroy

    respond_to do |format|
      format.html { redirect_to(evaluation_templates_url) }
      format.xml  { head :ok }
    end
  end


  private
  def set_evaluation_template
    @evaluation_template = EvaluationTemplate.find(params[:id])
  end

  def evaluation_template_params
    params.require(:evaluation_template).permit(:name, :logo)
  end
end
