class EvaluateesController < ApplicationController
  before_filter :authenticate_evaluator
  before_action :set_evaluatee, only: [:show, :edit, :update, :destroy]

  # GET /evaluatees
  # GET /evaluatees.json
  def index
    @evaluatees = current_user.evaluatees
  end

  # GET /evaluatees/1
  # GET /evaluatees/1.json
  def show
  end

  # GET /evaluatees/new
  def new
    @evaluatee = Evaluatee.new
  end

  # GET /evaluatees/1/edit
  def edit
  end

  # POST /evaluatees
  # POST /evaluatees.json
  def create
    @evaluatee = Evaluatee.new(evaluatee_params)
    @evaluatee.evaluator  = current_user

    respond_to do |format|
      if @evaluatee.save
        format.html { redirect_to @evaluatee, notice: 'Evaluatee was successfully created.' }
        format.json { render :show, status: :created, location: @evaluatee }
      else
        format.html { render :new }
        format.json { render json: @evaluatee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /evaluatees/1
  # PATCH/PUT /evaluatees/1.json
  def update
    respond_to do |format|
      if @evaluatee.update(evaluatee_params)
        format.html { redirect_to @evaluatee, notice: 'Evaluatee was successfully updated.' }
        format.json { render :show, status: :ok, location: @evaluatee }
      else
        format.html { render :edit }
        format.json { render json: @evaluatee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /evaluatees/1
  # DELETE /evaluatees/1.json
  def destroy
    @evaluatee.destroy
    respond_to do |format|
      format.html { redirect_to evaluatees_url, notice: 'Evaluatee was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_evaluatee
      @evaluatee = Evaluatee.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def evaluatee_params
      params.require(:evaluatee).permit(:name, :email)
    end
end
