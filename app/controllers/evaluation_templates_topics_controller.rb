class EvaluationTemplatesTopicsController < ApplicationController
  before_filter :authenticate_admin
  before_filter :set_evaluation_template_topic, only: [:show, :edit, :update, :destroy]

  # GET /evaluation_templates_topics
  # GET /evaluation_templates_topics.xml
  def index
    @evaluation_templates_topics = EvaluationTemplatesTopic.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @evaluation_templates_topics }
    end
  end

  # GET /evaluation_templates_topics/1
  # GET /evaluation_templates_topics/1.xml
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @evaluation_templates_topic }
    end
  end

  # GET /evaluation_templates_topics/new
  # GET /evaluation_templates_topics/new.xml
  def new
    @evaluation_templates_topic = EvaluationTemplatesTopic.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @evaluation_templates_topic }
    end
  end

  # GET /evaluation_templates_topics/1/edit
  def edit
  end

  # POST /evaluation_templates_topics
  # POST /evaluation_templates_topics.xml
  def create
    @evaluation_templates_topic = EvaluationTemplatesTopic.new(evaluation_template_topic_params)

    respond_to do |format|
      if @evaluation_templates_topic.save
        format.html { redirect_to(@evaluation_templates_topic, :notice => 'Evaluation templates topic was successfully created.') }
        format.xml  { render :xml => @evaluation_templates_topic, :status => :created, :location => @evaluation_templates_topic }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @evaluation_templates_topic.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /evaluation_templates_topics/1
  # PUT /evaluation_templates_topics/1.xml
  def update
    respond_to do |format|
      if @evaluation_templates_topic.update_attributes(evaluation_template_topic_params)
        format.html { redirect_to(@evaluation_templates_topic, :notice => 'Evaluation templates topic was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @evaluation_templates_topic.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /evaluation_templates_topics/1
  # DELETE /evaluation_templates_topics/1.xml
  def destroy
    @evaluation_templates_topic.destroy

    respond_to do |format|
      format.html { redirect_to(evaluation_templates_topics_url) }
      format.xml  { head :ok }
    end
  end


  private
  def set_evaluation_template_topic
    @evaluation_templates_topic = Comment.find(params[:id])
  end

#  evaluation_template_id :integer          not null
#  topic_id               :integer
  def evaluation_template_topic_params
    params.require(:evaluation_template_topic).permit(:topic_id, :evaluation_template_id)
  end
end
