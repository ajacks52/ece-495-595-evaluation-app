class TopicsController < ApplicationController
  before_filter :authenticate_admin
  before_filter :set_topic, only: [:show, :edit, :update, :destroy]

  # GET /topics
  # GET /topics.xml
  def index
    @topics = Topic.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @topics }
    end
  end

  # GET /topics/1
  # GET /topics/1.xml
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @topic }
    end
  end

  # GET /topics/new
  # GET /topics/new.xml
  def new
    @topic = Topic.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @topic }
    end
  end

  # GET /topics/1/edit
  def edit
  end

  # POST /topics
  # POST /topics.xml
  def create
    @topic = Topic.new(topic_params)
    @evaluation_template = EvaluationTemplate.find(params[:topic][:evaluation_template_id]) if params[:topic][:evaluation_template_id]

    respond_to do |format|
      if @topic.save
        if @evaluation_template
          @topic.evaluation_templates << @evaluation_template
        end

        if params[:topic][:parent_topic_id]
          parent_topic = Topic.find(params[:topic][:parent_topic_id])
          @topic.move_to_child_of(parent_topic)
        end

        format.html { redirect_to (@evaluation_template ? edit_evaluation_template_path(@evaluation_template) : topic_path(@topic)), :notice => 'Topic was successfully created.' }
        format.xml  { render :xml => @topic, :status => :created, :location => @topic }
        format.js
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @topic.errors, :status => :unprocessable_entity }
        format.js
      end
    end
  end

  # PUT /topics/1
  # PUT /topics/1.xml
  def update

    respond_to do |format|
      if @topic.update_attributes(topic_params)

        format.html { redirect_to(@topic, :notice => 'Topic was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @topic.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /topics/1
  # DELETE /topics/1.xml
  def destroy
    @topic.destroy

    respond_to do |format|
      format.html { redirect_to(topics_url) }
      format.xml  { head :ok }
    end
  end


  private

  def set_topic
    @topic = Topic.find(params[:id])
  end

  def topic_params
    params.require(:topic).permit(:rating_template_id, :title, :description, :parent_id, :lft, :rgt)
  end
end
