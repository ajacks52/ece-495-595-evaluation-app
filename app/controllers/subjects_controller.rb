class SubjectsController < ApplicationController
  before_filter :authenticate_admin
  before_filter :set_subject, only: [:show, :edit, :update, :destroy]

  # GET /subjects
  # GET /subjects.xml
  def index
    @subjects = Subject.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @subjects }
    end
  end

  # GET /subjects/1
  # GET /subjects/1.xml
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @subject }
    end
  end

  # GET /subjects/new
  # GET /subjects/new.xml
  def new
    @subject = Subject.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @subject }
    end
  end

  # GET /subjects/1/edit
  def edit
  end

  # POST /subjects
  # POST /subjects.xml
  def create
    @subject = Subject.new(evaluator_params)

    respond_to do |format|
      if @subject.save
        format.html { redirect_to(@subject, :notice => 'Subject was successfully created.') }
        format.xml  { render :xml => @subject, :status => :created, :location => @subject }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @subject.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /subjects/1
  # PUT /subjects/1.xml
  def update
    respond_to do |format|
      if @subject.update_attributes(evaluator_params)
        format.html { redirect_to(@subject, :notice => 'Subject was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @subject.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /subjects/1
  # DELETE /subjects/1.xml
  def destroy
    @subject.destroy

    respond_to do |format|
      format.html { redirect_to(subjects_url) }
      format.xml  { head :ok }
    end
  end


  private
  def set_subject
    @subject = Subject.find(params[:id])
  end

  def evaluator_params
    params.require(:subject).permit(:name)
  end
end
