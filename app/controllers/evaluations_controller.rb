class EvaluationsController < ApplicationController
  before_filter :authenticate_evaluator, except: [:preview]
  before_filter :set_evaluation, only: [:show, :edit, :update, :destroy]

  # GET /evaluations
  # GET /evaluations.xml
  def index
    @evaluations = Evaluation.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @evaluations }
    end
  end

  # GET /evaluations/1
  # GET /evaluations/1.xml
  def show
    @evaluatee = Evaluatee.find(params[:evaluatee_id]) if params[:evaluatee_id]
    @evaluation_template = EvaluationTemplate.find(params[:evaluation_template_id]) if params[:evaluation_template_id]
    @topics = @evaluation_template.topics.where(parent_id: nil)
    @topic = Topic.new

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @evaluation }
    end
  end

  # GET /evaluations/new
  # GET /evaluations/new.xml
  def new

    @evaluation = Evaluation.new
    @evaluatee = Evaluatee.find(params[:evaluatee_id]) if params[:evaluatee_id]
    @evaluation_template = EvaluationTemplate.find(params[:evaluation_template_id]) if params[:evaluation_template_id]
    @topics = @evaluation_template.topics.where(parent_id: nil)
    @topic = Topic.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @evaluation }
    end
  end

  # GET /evaluations/1/edit
  def edit
    @evaluation_template = EvaluationTemplate.find(@evaluation.evaluation_template_id)
    @subject = Subject.find(@evaluation.subject_id)
    @evaluator = Evaluator.find(@evaluation.evaluator_id)
  end

  # POST /evaluations
  # POST /evaluations.xml
  def create
    @evaluation = Evaluation.new(evaluation_params)

    respond_to do |format|
      if @evaluation.save
        format.html { redirect_to(@evaluation, :notice => 'Evaluation was successfully created.') }
        format.xml  { render :xml => @evaluation, :status => :created, :location => @evaluation }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @evaluation.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /evaluations/1
  # PUT /evaluations/1.xml
  def update
    respond_to do |format|
      if @evaluation.update_attributes(evaluation_params)
        format.html { redirect_to(@evaluation, :notice => 'Evaluation was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @evaluation.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /evaluations/1
  # DELETE /evaluations/1.xml
  def destroy
    @evaluation.destroy

    respond_to do |format|
      format.html { redirect_to(evaluations_url) }
      format.xml  { head :ok }
    end
  end

  # GET "evaluations/:evaluation_id/preview" => "evaluations#preview", as: 'evaluation_preview'
  def preview
    @evaluation = Evaluation.find(params[:evaluation_id])
    @evaluatee = @evaluation.evaluatee
    @evaluator = @evaluation.evaluator
    @evaluation_template = @evaluation.evaluation_template
    @topics = @evaluation_template.topics.where(parent_id: nil)
  end

  private
  def set_evaluation
    @evaluation = Evaluation.find(params[:id])
  end

  def evaluation_params
    params.require(:evaluation).permit(:evaluation_template_id, :evaluator_id, :subject_id, :evaluation_date)
  end
end
