class RatingTemplatesController < ApplicationController
  before_filter :authenticate_admin
  before_filter :set_rating_template, only: [:show, :edit, :update, :destroy]

  # GET /rating_templates
  # GET /rating_templates.xml
  def index
    @rating_templates = RatingTemplate.all

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @rating_templates }
    end
  end

  # GET /rating_templates/1
  # GET /rating_templates/1.xml
  def show
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @rating_template }
    end
  end

  # GET /rating_templates/new
  # GET /rating_templates/new.xml
  def new
    @rating_template = RatingTemplate.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @rating_template }
    end
  end

  # GET /rating_templates/1/edit
  def edit
  end

  # POST /rating_templates
  # POST /rating_templates.xml
  def create
    @rating_template = RatingTemplate.new(rating_template_params)

    respond_to do |format|
      if @rating_template.save
        format.html { redirect_to(@rating_template, :notice => 'Rating template was successfully created.') }
        format.xml  { render :xml => @rating_template, :status => :created, :location => @rating_template }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @rating_template.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /rating_templates/1
  # PUT /rating_templates/1.xml
  def update

    respond_to do |format|
      if @rating_template.update_attributes(rating_template_params)
        format.html { redirect_to(@rating_template, :notice => 'Rating template was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @rating_template.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /rating_templates/1
  # DELETE /rating_templates/1.xml
  def destroy
    @rating_template.destroy

    respond_to do |format|
      format.html { redirect_to(rating_templates_url) }
      format.xml  { head :ok }
    end
  end

  private
  def set_rating_template
    @rating_template = RatingTemplate.find(params[:id])
  end

  def rating_template_params
    params.require(:rating_template).permit(:low, :high, :low_description, :high_description)
  end
end
