json.array!(@evaluatees) do |evaluatee|
  json.extract! evaluatee, :id, :name, :email
  json.url evaluatee_url(evaluatee, format: :json)
end
